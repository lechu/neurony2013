package pl.edu.agh.sn;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import pl.edu.agh.sn.activation.AFApplier;
import pl.edu.agh.sn.activation.LinearActivationFunction;
import pl.edu.agh.sn.activation.SigmoidActivationFunction;
import pl.edu.agh.sn.activation.StepActivationFunction;


public class NetworkBuilder {
	
	private static final String FILENAME = "test/xor";
	
	private static List<NeuronLayer> layers = new LinkedList<NeuronLayer>();
	
	public static void main (String[] args){
		
		NetworkBuilder nb = new NetworkBuilder();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(FILENAME));

			nb.buildLayers(br);
			
			nb.buildConnections(br);

			nb.buildActivationFunction(br);
			
			nb.processInput(br);
			
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private void train(int epochs, double learningRate, double momentum, List<Double> inputs, List<Double> outputs){
		
	}

	private void buildActivationFunction(BufferedReader br) throws IOException {
		System.out.print("Activation function [PURELIN / tansig / hardlim]: ");
		String function = br.readLine();
		System.out.println(function);
		if (function.equals("tansig")){
			AFApplier.setFunction(new SigmoidActivationFunction(2.0, 2.0, 1.0));
		} else if (function.equals("hardlim")){
			AFApplier.setFunction(new StepActivationFunction(0.0));
		} else {
			AFApplier.setFunction(new LinearActivationFunction(1.0, 0.0));
		}
		
	}

	private void processInput(BufferedReader br) throws NumberFormatException, IOException {
		List <Double> input = new LinkedList <Double>();
		for (Integer i = 0; i < layers.get(0).getInputCount() ; i++){
			System.out.print("Input value #"+i+": ");
			Double read = Double.parseDouble(br.readLine());
			System.out.println(read);
			input.add(read);
		}
		layers.get(0).process(input);
	}

	private void buildConnections(BufferedReader br) throws NumberFormatException, IOException {
		System.out.print("Generate random weights and biases? [y/N]: ");
		if (br.readLine().equals("y")) {
			System.out.println("y");
			buildNeurons(br, true);
		} else {
			System.out.println("n");
			buildNeurons(br, false);
		}
	}

	private void buildNeurons(BufferedReader br, Boolean random) throws NumberFormatException, IOException {	
		for (NeuronLayer layer : layers){
			Integer neuronsCount = layer.getNeuronsCount();
			for (Integer i = 0 ; i < neuronsCount ; i++){
				Neuron neuron = new Neuron();
				neuron.setLayer(layer);
				Double bias;
				if (!random){
					System.out.print ("Bias: ");
					bias = Double.parseDouble(br.readLine());
				} else {
					bias = RandomValuesGenerator.getRandomBias();
				}
				System.out.println(bias);
				neuron.setBias(bias);
				for (Integer j = 0; j < layer.getInputCount() ; j++){
					Double weight;
					if (!random){
						System.out.print ("Weight: ");
						weight = Double.parseDouble(br.readLine());
					} else {
						weight = RandomValuesGenerator.getRandomWeight();
					}
					System.out.println(weight);
					neuron.addWeight(weight);
				}
				layer.addNeuron(neuron)	;
			}
		}
		
	}

	private void buildLayers(BufferedReader br) throws NumberFormatException, IOException {
		System.out.print("Number of inputs: ");
		Integer inputsCount = Integer.parseInt(br.readLine());
		System.out.println(inputsCount);
		System.out.print("Number of layers: ");
		Integer layersCount = Integer.parseInt(br.readLine());
		System.out.println(layersCount);
		NeuronLayer firstLayer = null;
		NeuronLayer previousLayer = null;
		for (Integer i = 0; i < layersCount ; i++){
			System.out.print("Number of neurons in layer " + i + ": ");
			NeuronLayer layer = new NeuronLayer();
			layer.setNeuronsCount(Integer.parseInt(br.readLine()));
			System.out.println(layer.getNeuronsCount());
			if (null == firstLayer){
				layer.setInputCount(inputsCount);
				firstLayer = layer;
			} else {
				layer.appendSelfToLayer(previousLayer);
				layer.setInputCount(previousLayer.getNeuronsCount());
			}
			previousLayer = layer;
			layers.add(layer);
		}
	}
}
