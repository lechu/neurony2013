package pl.edu.agh.sn;

import java.util.LinkedList;
import java.util.List;

import pl.edu.agh.sn.activation.AFApplier;

public class Neuron {
	private Double bias = 0.0;
	private NeuronLayer layer;
	private List <Double> weights = new LinkedList <Double>();

	public void setBias(double bias) {
		this.bias=bias;
	}

	public void setLayer(NeuronLayer layer) {
		this.layer = layer;
	}

	public void addWeight(double weight) {
		weights.add(weight);
	}

	public Double process(List<Double> input) {
		Double score = bias;
		for (Integer i = 0 ; i < weights.size() ; i++){
			score += weights.get(i)*input.get(i);
		}
		return AFApplier.apply(score);
	}
}
