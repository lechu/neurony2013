package pl.edu.agh.sn;

import java.util.LinkedList;
import java.util.List;

public class NeuronLayer {
	private Integer neuronsCount;
	private NeuronLayer previousLayer;
	private NeuronLayer nextLayer;
	private Integer inputCount;
	
	public Integer getInputCount() {
		return inputCount;
	}

	private List <Neuron> neurons = new LinkedList<Neuron>();
	
	public void setNeuronsCount (Integer neuronsCount){
		this.neuronsCount = neuronsCount;
	}
	
	public void addNeuron (Neuron neuron){
		neurons.add(neuron);
	}
	
	public void appendSelfToLayer (NeuronLayer layer){
		layer.setNextLayer(this);
		this.previousLayer = layer;
	}

	public Integer getNeuronsCount() {
		return neuronsCount;
	}

	private void setNextLayer(NeuronLayer nextLayer) {
		this.nextLayer = nextLayer;
	}

	public void setInputCount(Integer inputCount) {
		this.inputCount = inputCount;
	}

	public void process(List<Double> input) {
		List <Double> output = new LinkedList <Double>();
		for (Neuron neuron : neurons){
			output.add(neuron.process(input));
		}
		if (null != nextLayer) {
			//printOutput();
			nextLayer.process(output);
		} else {
			printOutput(output);
		}
		
	}
	
	private void printOutput (List <Double> output){
		System.out.println("Output is:");
		for (Double d : output){
			System.out.println(d);
		}
	}
}
