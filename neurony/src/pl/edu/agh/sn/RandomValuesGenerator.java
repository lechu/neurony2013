package pl.edu.agh.sn;

import java.util.Random;

public class RandomValuesGenerator {
	
	private static Random r = new Random();
	private static Double rangeMin = 0.0;
	private static Double rangeMax = 1.0;

	
	public static Double getRandomBias (){
		return rangeMin + (rangeMax - rangeMin) * r.nextDouble();
	}
	
	public static Double getRandomWeight (){
		return rangeMin + (rangeMax - rangeMin) * r.nextDouble();
	}
	
	public static int nextInt() {
		return r.nextInt();
	}

	public static int nextInt(int size) {
		return r.nextInt(size);
	}
	
}
