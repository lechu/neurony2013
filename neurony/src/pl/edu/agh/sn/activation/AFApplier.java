package pl.edu.agh.sn.activation;

public class AFApplier {
	private static IActivationFunction function;
	
	public static void setFunction(IActivationFunction function) {
		AFApplier.function = function;
	}

	public static Double apply (Double x){
		return function.apply(x);
	}
}
