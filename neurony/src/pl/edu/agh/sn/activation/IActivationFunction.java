package pl.edu.agh.sn.activation;

public interface IActivationFunction {
	public double apply(Double x);

	public double calculateDerivativeOfActivation(double d);
}
