package pl.edu.agh.sn.activation;

public class LinearActivationFunction implements IActivationFunction {

	private Double a, b;
	
	
	public LinearActivationFunction(Double a, Double b) {
		super();
		this.a = a;
		this.b = b;
	}


	@Override
	public double apply(Double x) {
		return a*x + b;
	}


	@Override
	public double calculateDerivativeOfActivation(double d) {
		return 1;
	}

}
