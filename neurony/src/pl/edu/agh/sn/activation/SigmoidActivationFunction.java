package pl.edu.agh.sn.activation;

public class SigmoidActivationFunction implements IActivationFunction {

	private Double alpha, beta, gamma;


	public SigmoidActivationFunction(Double alpha, Double beta, Double gamma) {
		super();
		this.alpha = alpha;
		this.beta = beta;
		this.gamma = gamma;
	}


	@Override
	public double apply(Double x) {
		//System.out.print (alpha + " / ( 1 + e^(-" + beta + "*" + x + "))) - " + gamma + " = ");
		Double score = alpha / (1 + Math.exp(-beta*x)) - gamma;
		//System.out.println(score);
		return score;
	}


	@Override
	public double calculateDerivativeOfActivation(double d) {
		return (1-d)*d;
	}

}
