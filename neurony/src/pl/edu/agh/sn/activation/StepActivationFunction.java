package pl.edu.agh.sn.activation;



public class StepActivationFunction implements IActivationFunction {

	private Double xLim;
	
	public StepActivationFunction(Double xLim) {
		super();
		this.xLim = xLim;
	}
	@Override
	public double apply(Double x) {
		if (x > xLim) 
			return 1.0;
		return 0.0;
	}
	@Override
	public double calculateDerivativeOfActivation(double d) {
		return 1;
	}
	
	

}
