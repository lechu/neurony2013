package pl.edu.agh.sn.backpropagation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.TreeMap;

import pl.edu.agh.sn.activation.IActivationFunction;
import pl.edu.agh.sn.activation.LinearActivationFunction;
import pl.edu.agh.sn.activation.SigmoidActivationFunction;
import pl.edu.agh.sn.activation.StepActivationFunction;
import pl.edu.agh.sn.initializestrategy.InitialValueStrategy;
import pl.edu.agh.sn.initializestrategy.RandomInitialStrategy;
import pl.edu.agh.sn.initializestrategy.ZeroInitialStrategy;

public class Main {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		String networkType = args[0];
		String settingsFileName = args[1];
		String trainingFileName = args[2];
		String trainingConfFileName = args[3];
		String vectorFileName = args[4];

		List<SimpleNeuronLayerBuilder> settings = parseStructureConfiguration(new File(
				settingsFileName));

		List<TrainingData> data = parseTrainingData(new File(trainingFileName));

		SimpleTrainingConfiguration configuration = parseTrainingConfiguration(new File(
				trainingConfFileName));

		double[][] inputVectors = parseInputVectors(new File(vectorFileName));

		SimpleNeuralNetwork nn = getNeuralNetwork(settings);

		nn.train(data, configuration);

		for(int i = 0; i < inputVectors.length; i++) {
            double[] currentInput = inputVectors[i];
            System.out.println("Input: (" + Arrays.toString(currentInput) + ")");
            double[] result = nn.calculateOutput(currentInput);
            System.out.println("Output: (" + Arrays.toString(result) + ")");
            System.out.println();
        }

	}

	public static String asString(File file) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			StringBuilder fileContents = new StringBuilder();

			String line = "";
			while ((line = br.readLine()) != null) {
				fileContents.append(line + "\n");
			}

			return "" + fileContents;
		} catch (Exception ex) {
		} finally {
			try {
				if (br != null) {
					br.close();
				}
			} catch (Exception ex) {
			}
		}
		return null;
	}

	public static List<TrainingData> parseTrainingData(File inputFile) {
		List<TrainingData> result = new LinkedList<TrainingData>();
		String inputString = asString(inputFile);
		Scanner scanner = new Scanner(inputString);
		scanner.useLocale(new Locale("pl_PL"));

		// rozmiary wektorow wejsciowych i wyjsciowych
		int inputVectorSize = Integer.valueOf(scanner.nextInt());
		int outputVectorSize = Integer.valueOf(scanner.nextInt());
		scanner.nextLine();

		while (scanner.hasNextLine()) {
			double[] input = new double[inputVectorSize];
			for (int j = 0; j < inputVectorSize; j++)
				input[j] = scanner.nextDouble();

			double[] output = new double[outputVectorSize];
			for (int j = 0; j < outputVectorSize; j++)
				output[j] = scanner.nextDouble();

			TrainingData data = new TrainingData(input, output);
			result.add(data);

			scanner.nextLine();
		}
		return result;
	}

	public static SimpleTrainingConfiguration parseTrainingConfiguration(
			File inputFile) throws IOException {
		String inputString = asString(inputFile);
		Properties props = new Properties();
		props.load(new StringReader(inputString));

		// podstawowe
		int epochs = Integer.valueOf(props.getProperty("epochs"));
		// wartosci krotkowe
		TreeMap<Integer, Double> learningRate = parseTupleValue(props
				.getProperty("learningRate"));
		TreeMap<Integer, Double> momentum = parseTupleValue(props
				.getProperty("momentum"));

		return new SimpleTrainingConfiguration(epochs, learningRate, momentum);

	}

	// -- parsowanie krotek (momentum i learningRate)

	// stale pliku konfiguracyjnego
	private static final String TUPLE_DELIMITER = "\\|";
	private static final String TUPLE_BEGINNING = "(";
	private static final String VALUES_DELIMITER = ";";

	// krotka -> mapa parametru wzgledem numeru epoki
	private static TreeMap<Integer, Double> parseTupleValue(String string) {
		if (string == null || string.isEmpty())
			throw new IllegalArgumentException("null value for "
					+ "crucial training setting!");

		if (!string.contains(TUPLE_BEGINNING))
			return prepareOneValueTupleMap(string);

		TreeMap<Integer, Double> result = new TreeMap<Integer, Double>();

		// podzial lancucha na lancuchy krotek
		String[] tupleStrings = string.split(TUPLE_DELIMITER);
		for (String tupleString : tupleStrings) {
			// pozbywamy sie nawiasow
			String innerTuple = tupleString.substring(1);
			innerTuple = innerTuple.substring(0, innerTuple.length() - 1);

			String[] values = innerTuple.split(VALUES_DELIMITER);
			result.put(Integer.valueOf(values[0]), Double.valueOf(values[1]));

		}

		return result;
	}

	// odpowiednik starego modelu - parametr krotkowy niezmienny w czasie
	private static TreeMap<Integer, Double> prepareOneValueTupleMap(
			String string) {
		TreeMap<Integer, Double> result = new TreeMap<Integer, Double>();
		result.put(0, Double.valueOf(string));
		return result;
	}

	// //////////////////////////////////////////////////////////////////////////////////////

	public static List<SimpleNeuronLayerBuilder> parseStructureConfiguration(
			File inputFile) {

		String inputString = asString(inputFile);

		List<SimpleNeuronLayerBuilder> output = new LinkedList<SimpleNeuronLayerBuilder>();

		Scanner scanner = new Scanner(inputString);

		// rozmiar wektora wejsciowego
		int inputVectorSize = Integer.valueOf(scanner.nextLine());

		int previousLayerSize = inputVectorSize;
		while (scanner.hasNextLine()) {
			// ilosc neuronow w warstwie
			int neuronCount = scanner.nextInt();
			// funkcja aktywacji warstwy
			String activationString = scanner.next().toUpperCase();
			IActivationFunction activation = null;
			if ("SIGMOID".equals(activationString)) {
				activation = new SigmoidActivationFunction(2.0, 2.0, 1.0);
			} else if ("LINEAR".equals(activationString)) {
				activation = new LinearActivationFunction(1.0, 0.0);
			} else if ("THRESHOLD".equals(activationString)) {
				activation = new StepActivationFunction(0.0);
			}

			scanner.nextLine();

			// dwuwymiarowa tablica wag (mozliwe wartosci domyslne)
			double[][] weights = null;
			InitialValueStrategy weightsStrategy = getInitialValueStrategy(scanner);
			if (weightsStrategy == null) {
				weights = new double[neuronCount][previousLayerSize];
				for (int i = 0; i < neuronCount; i++) {
					StringTokenizer tokenizer = new StringTokenizer(
							scanner.nextLine());

					int j = 0;
					while (tokenizer.hasMoreElements()) {
						weights[i][j++] = Double.valueOf(tokenizer.nextToken());
					}
				}
			}

			// bias dla warstwy (mozliwe wartosci domyslne)
			double[] bias = null;

			InitialValueStrategy biasStrategy = null;
			// jesli bias wylaczony to pozostawiamy go nullem
			if (!checkNoBias(scanner)) {
				biasStrategy = getInitialValueStrategy(scanner);

				if (biasStrategy == null) {
					bias = new double[neuronCount];
					for (int i = 0; i < neuronCount; i++) {
						bias[i] = Double.valueOf(scanner.nextDouble());
						// bias[i] = 0.1;
					}
					scanner.nextLine();
				}
			}

			// utworzenie buildera dla aktualnej warstwy neuronow
			SimpleNeuronLayerBuilder builder = new SimpleNeuronLayerBuilder(
					neuronCount, previousLayerSize, activation);

			if (weightsStrategy == null) {
				builder.weights(weights);
			} else {
				builder.undefinedInitialWeights(weightsStrategy);
			}

			if (biasStrategy == null) {
				builder.bias(bias);
			} else {
				builder.undefinedInitialBias(biasStrategy);
			}

			output.add(builder);

			previousLayerSize = neuronCount;
		}

		return output;
	}

	private static final String RANDOM_INITIAL_STRING = "\\?";
	private static final String ZERO_INITIAL_STRING = "\\!";
	private static final String NO_BIAS_STRING = "x";

	/**
	 * Sprawdza czy zazadano wylaczenia biasu.
	 */
	private static boolean checkNoBias(Scanner scanner) {
		if (scanner.hasNext(NO_BIAS_STRING)) {
			scanner.nextLine();
			return true;
		}

		return false;
	}

	/**
	 * Zwraca strategie dla losowych wag poczatkowych (np. RANDOM dla "?" w
	 * pliku konfiguracyjnym) lub null, jesli wagi podano explicite.
	 */
	private static InitialValueStrategy getInitialValueStrategy(Scanner scanner) {
		if (scanner.hasNext(ZERO_INITIAL_STRING)) {
			scanner.nextLine();
			return new ZeroInitialStrategy();
		} else if (scanner.hasNext(RANDOM_INITIAL_STRING)) {
			scanner.next();
			RandomInitialStrategy value = null;
			if (scanner.hasNextDouble()) {
				// double a = scanner.nextDouble();
				// double b = scanner.nextDouble();
				value = new RandomInitialStrategy();
			} else {
				value = new RandomInitialStrategy();
			}
			scanner.nextLine();
			return value;
		}

		return null;
	}

	public static double[][] parseInputVectors(File inputFile)
			throws IOException {
		double[][] output;
		String inputString = asString(inputFile);

		Scanner scanner = new Scanner(inputString);
		scanner.useLocale(new Locale("pl_PL"));
		// rozmiar wektorow wejsciowych
		int lines = countLines(inputString) - 1; // jeden na naglowek
		int inputVectorSize = Integer.valueOf(scanner.nextLine());

		// wartosci
		output = new double[lines][];
		for (int i = 0; i < lines; i++) {
			output[i] = new double[inputVectorSize];
			for (int j = 0; j < inputVectorSize; j++)
				output[i][j] = scanner.nextDouble();
			scanner.nextLine();
		}

		return output;
	}

	public static int countLines(String string) throws IOException {
		int count = 0;

		BufferedReader br = new BufferedReader(new StringReader(string));
		while (br.readLine() != null)
			count++;

		return count;
	}

	private static SimpleNeuralNetwork getNeuralNetwork(
			List<SimpleNeuronLayerBuilder> settings) {
		return new SimpleNeuralNetwork(settings);
	}

}
