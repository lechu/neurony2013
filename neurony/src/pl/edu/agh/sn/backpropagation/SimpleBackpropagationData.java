package pl.edu.agh.sn.backpropagation;

import java.util.Arrays;

public class SimpleBackpropagationData {
	double[][] weights;
    double[] errors;

    public SimpleBackpropagationData(double[][] weights, double[] errors) {
        if(weights == null || errors == null)
            throw new IllegalArgumentException();

        this.weights = weights;
        this.errors = errors;
    }

    public double[][] getWeights() {
        return copyArray(weights);
    }

    public double[] getErrors() {
        return Arrays.copyOf(errors, errors.length);
    }

    /**
     * Zwraca wagi laczace neurony warstwy "kolejnej" z neuronem "j" warstwy
     * "poprzedniej".
     */
    public double[] getWeightsForPreviousNeuron(int j) {
        double[] jNeuronWeights = new double[weights.length];
        for(int i = 0; i < weights.length; i++)
            jNeuronWeights[i] = weights[i][j];
        return jNeuronWeights;
    }
    
    public double[][] copyArray(double[][] array) {
		if (array == null)
			return null;

		double[][] result = new double[array.length][];
		for (int i = 0; i < array.length; i++)
			result[i] = Arrays.copyOf(array[i], array[i].length);
		return result;
	}
}
