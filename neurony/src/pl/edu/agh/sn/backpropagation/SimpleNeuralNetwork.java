package pl.edu.agh.sn.backpropagation;

import java.util.Collections;
import java.util.List;

public class SimpleNeuralNetwork {

    public SimpleNeuralNetwork(List<SimpleNeuronLayerBuilder> list) {

        if (list.size() < 1) {
            throw new IllegalArgumentException();
        }

        neuronLayers = new SimpleNeuronLayer[list.size()];
        for (int i = 0; i < list.size(); i++) {
            neuronLayers[i] = new SimpleNeuronLayer(list.get(i));
        }

    }

    public double[] calculateOutput(double[] input) {
        double[] currentInput = input;
        for (int i = 0; i < neuronLayers.length; i++) {
            currentInput = neuronLayers[i].calculateOutput(currentInput);
        }

        return currentInput;
    }

    public void train(List<TrainingData> trainingData, SimpleTrainingConfiguration settings) {

        for (int i = 0; i < settings.getEpochs(); i++) {

            // learningRate i momentum dla aktualnej epoki
            double learningRate = settings.getLearningRate(i);
            double momentum = settings.getMomentum(i);

            // losowa kolejnosc wektorow wejsciowych w epoce
            Collections.shuffle(trainingData);

            double epochError = 0.0;

            for (int j = 0; j < trainingData.size(); j++) {

                // aktualne dane nauki
                double[] currentInput = trainingData.get(j).getInput();
                double[] expectedOutput = trainingData.get(j).getOutput();

                // obliczamy wyjscie sieci
                double[] computedOutput = calculateOutput(currentInput);

                // aktualizacja bledu sredniokwadratowego
                epochError += calculateMeanError(computedOutput, expectedOutput);

                // backpropagation
                SimpleBackpropagationData backpropagationData = null;
                for (int k = neuronLayers.length - 1; k >= 0; k--) {
                    backpropagationData = neuronLayers[k].actualizeWeights(expectedOutput,
                            learningRate, momentum, backpropagationData);
                }

            }
        }
    }

    /**
     * Wylicza blad sredniokwadratowy dla dwoch podanych wektorow.
     */
    private double calculateMeanError(double[] calculated, double[] expected) {
        double sum = 0;
        for (int i = 0; i < calculated.length; i++) {
            sum += Math.pow(expected[i] - calculated[i], 2);
        }
        return Math.sqrt(sum / calculated.length);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (SimpleNeuronLayer layer : neuronLayers) {
            builder.append("Layer:\n");
            
            for (double[] neuronWeights : layer.getWeights()) {
                for (double weight : neuronWeights) {
                    builder.append(String.format("%7.2f", weight));
                }
                builder.append("\n");
            }
            builder.append("  --\n");
            
            if (layer.getBias() != null) {
                for (double bias : layer.getBias()) {
                    builder.append(String.format("%7.2f", bias));
                }
            } else
                builder.append("No Bias");
            builder.append("\n");
        }
        return "" + builder;
    }
    private SimpleNeuronLayer[] neuronLayers;
}
