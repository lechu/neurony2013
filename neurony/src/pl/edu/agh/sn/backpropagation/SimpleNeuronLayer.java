package pl.edu.agh.sn.backpropagation;

import java.util.Arrays;

import pl.edu.agh.sn.activation.IActivationFunction;

public class SimpleNeuronLayer {

    /**
     * Standardowa inicjalizacja warstwy neuronow na podstawie obiektu
     * Builder.
     */
    public SimpleNeuronLayer(SimpleNeuronLayerBuilder builder) {
        this.neuronCount = builder.getNeuronCount();
        this.previousNeuronCount = builder.getPreviousNeuronCount();

        this.activationType = builder.getActivationType();

        this.weights = builder.getWeights();
        this.lastWeightDeltas = new double[neuronCount][];
        for(int i = 0; i < neuronCount; i++)
            lastWeightDeltas[i] = new double[previousNeuronCount];

        this.bias = builder.getBias();
        if(this.bias != null)
            this.lastBiasDeltas = new double[neuronCount];
    }

    public double[] calculateOutput(double[] input) {
        if (input == null || input.length != previousNeuronCount) {
            throw new IllegalArgumentException("wrong input vector's size!");
        }

        // zapamietujemy ostatnie wejscie...
        lastInput = Arrays.copyOf(input, input.length);

        // i ostatnie wyjscie...
        lastOutput = new double[neuronCount];
        for (int i = 0; i < lastOutput.length; i++) {
            double currentOutput = bias == null ? 0.0 : bias[i];

            for (int j = 0; j < input.length; j++) {
                currentOutput += input[j] * weights[i][j];
            }

            lastOutput[i] = activationType.apply(currentOutput);
        }

        return lastOutput;
    }
    private int neuronCount;
    private int previousNeuronCount;

    private double[][] weights;

    private IActivationFunction activationType;

    private double[] bias;

    // wejscia ostatniego przebiegu calculateOutput
    private double[] lastInput;
    // wyjscia ostatniego przebiegu calculateOutput
    private double[] lastOutput;

    // zmiany wag z ostatniego obiegu train()
    private double[][] lastWeightDeltas;
    // zmiany biasu z ostatniego obiegu train()
    private double[] lastBiasDeltas;

    public double[] getBias() {
        if (bias == null)
            return null;

        return Arrays.copyOf(bias, bias.length);
    }

    /*
     * aktualizuje wagi warstwy w pojedynczym kroku nauki; zwraca dane niezbedne
     * do nauczenia warstwy poprzedniej
     */
    public SimpleBackpropagationData actualizeWeights(double[] expectedOutput, 
                            double learningRate, double momentum,
                            SimpleBackpropagationData backpropagationData) {

        // dane nauczania dla wczesniejszej warstwy (jesli istnieje)
        double[] errors = new double[neuronCount];
        // kopia wag sprzed aktualizacji
        double[][] beforeWeights = getWeights();

        for (int i = 0; i < neuronCount; i++) {

            double sigma = 0;
            // warstwy ukryte
            if (backpropagationData != null) {
                double[] nextLayerWeights = backpropagationData.getWeightsForPreviousNeuron(i);
                double[] nextLayerErrors = backpropagationData.getErrors();
                for (int j = 0; j < nextLayerWeights.length; j++) {
                    sigma += nextLayerWeights[j] * nextLayerErrors[j];
                }

            // warstwa wyjsciowa
            } else {
                sigma = expectedOutput[i] - lastOutput[i];
            }

            double derivative = activationType.calculateDerivativeOfActivation(lastOutput[i]);
            double error = sigma * derivative;

            // zapamietanie bledu
            errors[i] = error;

            // aktualizacja wag
            for (int j = 0; j < previousNeuronCount; j++) {

                double deltaW = learningRate * error * lastInput[j];

                actualizeWeight(i, j, deltaW, momentum);

            }

            // aktualizacja biasu
            if (bias != null)
                actualizeBias(i, error * learningRate, momentum);

        }

        return new SimpleBackpropagationData(beforeWeights, errors);

    }

    void actualizeWeight(int i, int j, double deltaW, double momentum) {

        // pierwszy obieg nic nie psuje - lastWeightDeltas = 0
        double fullDelta = deltaW + momentum * lastWeightDeltas[i][j];

        weights[i][j] += fullDelta;
        lastWeightDeltas[i][j] = fullDelta;

    }

    void actualizeBias(int i, double deltaB, double momentum) {

        double fullDelta = deltaB + momentum * lastBiasDeltas[i];

        bias[i] += fullDelta;
        lastBiasDeltas[i] = fullDelta;

    }

    public double[][] getWeights() {
        return copyArray(weights);
    }
    
    public double[][] copyArray(double[][] array) {
		if (array == null)
			return null;

		double[][] result = new double[array.length][];
		for (int i = 0; i < array.length; i++)
			result[i] = Arrays.copyOf(array[i], array[i].length);
		return result;
	}
}
