package pl.edu.agh.sn.backpropagation;

import java.util.Arrays;

import pl.edu.agh.sn.activation.IActivationFunction;
import pl.edu.agh.sn.initializestrategy.InitialValueStrategy;
import pl.edu.agh.sn.kohonen.KohonenNeuronLayerBuilder;

public class SimpleNeuronLayerBuilder extends KohonenNeuronLayerBuilder {
    private IActivationFunction activationType;
    private double[] bias;
    private SimpleNeuralNetwork neuralNetwork;

    public IActivationFunction getActivationType() {
        return activationType;
    }

    public double[] getBias() {
        if (bias == null)
            return null;
        
        return Arrays.copyOf(bias, bias.length);
    }

    // mandatory part of initializing a neuron layer
    public SimpleNeuronLayerBuilder(int neuronCount, int previousNeuronCount,
            IActivationFunction activationType) {
        super(neuronCount, previousNeuronCount);

        if (activationType == null) {
            throw new IllegalArgumentException("incorrect layer settings!");
        }

        this.activationType = activationType;
    }

    public void bias(double[] bias) {
        if (bias != null && getNeuronCount() != bias.length) {
            throw new IllegalArgumentException("incorrect bias size");
        }

        this.bias = bias;
    }

    public void undefinedInitialBias(InitialValueStrategy nullStrategy) {
        bias = new double[getNeuronCount()];
        for (int i = 0; i < getNeuronCount(); i++) {
            bias[i] = nullStrategy.getValue();
        }
    }
    
    public void weights(double[][] weights) {
        if (weights == null) {
            throw new IllegalArgumentException("if you don't want to define"
                    + "weights, use undefinedInitialWeights method!");
        }

        if (getNeuronCount() != weights.length) {
            throw new IllegalArgumentException("incorrect weights size");
        }
        for (int i = 0; i < weights.length; i++) {
            if (weights[i].length != getPreviousNeuronCount()) {
                throw new IllegalArgumentException("incorrect weights size");
            }
        }

        this.weights = weights;
    }
    
    public void undefinedInitialWeights(InitialValueStrategy nullStrategy) {
        weights = new double[getNeuronCount()][];
        for (int i = 0; i < getNeuronCount(); i++) {
            weights[i] = new double[getPreviousNeuronCount()];
        }

        for (int i = 0; i < getWeights().length; i++) {
            for (int j = 0; j < getWeights()[i].length; j++) {
                weights[i][j] = nullStrategy.getValue();
            }
        }
    }
}
