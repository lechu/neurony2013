package pl.edu.agh.sn.backpropagation;

import java.util.Map;
import java.util.TreeMap;

public class SimpleTrainingConfiguration {
    private final int epochs;

    private final TreeMap<Integer, Double> learningRate;
    private final TreeMap<Integer, Double> momentum;

    public SimpleTrainingConfiguration(int epochs, 
                       TreeMap<Integer, Double> learningRate,
                       TreeMap<Integer, Double> momentum) {
        if(learningRate.isEmpty() || momentum.isEmpty())
            throw new IllegalArgumentException("you must provide"
                    + " learningRate and momentum!");

        this.epochs = epochs;
        this.learningRate = learningRate;
        this.momentum = momentum;
    }

    /**
     * Zazadana ilosc epok procesu nauczania.
     */
    public int getEpochs() {
        return epochs;
    }
    /**
     * Pobranie learningRate dla epoki epoch.
     */
    public double getLearningRate(int epoch) {
        return getValueForEpoch(learningRate, epoch);
    }
    /**
     * Pobranie momentum dla epoki epoch.
     */
    public double getMomentum(int epoch) {
        return getValueForEpoch(momentum, epoch);
    }

    private double getValueForEpoch(TreeMap<Integer, Double> map, int epoch) {
        double result = 0.0;

        // zaklada posortowana mape - stad wymaganie TreeMap
        // aktualizujemy wspolczynnik dopoki pozostajemy w zakresie (epoka)
        for(Map.Entry<Integer, Double> entry : map.entrySet()) {
            if(entry.getKey() > epoch)
                break;

            result = entry.getValue();
        }

        return result;
    }

    @Override
    public String toString() {
        return "epochs: " + epochs + ", " +
               "learningRate: " + learningRate + ", " +
               "momentum: " + momentum;
    }
}
