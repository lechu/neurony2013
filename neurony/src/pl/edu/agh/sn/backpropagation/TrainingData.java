package pl.edu.agh.sn.backpropagation;

import java.util.Arrays;

public class TrainingData {
	private double[] input;
    private double[] output;

    public TrainingData(double[] input, double[] output) {
        this.input = input;
        this.output = output;
    }

    public double[] getInput() {
        return Arrays.copyOf(input, input.length);
    }

    public void setInput(double[] input){
        this.input = input;
    }

    public double[] getOutput() {
        return Arrays.copyOf(output, output.length);
    }

}
