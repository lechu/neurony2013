package pl.edu.agh.sn.cp;

import java.util.ArrayList;
import java.util.List;

public class GrossBergNeuron {
	
	private List <Double> weights = new ArrayList <Double>(); 
	
	public GrossBergNeuron(int inputCount) {
		for (int i = 0 ; i < inputCount ; i++)
			weights.add(0.0);
	}
	
	public Double teach (double[] inputs, Double expectedOutput, Double alpha){
		Double output = 0.0;
		for (int i = 0 ; i < inputs.length ; i++){
			output += weights.get(i) * inputs[i];
		}
		int winnerIndex=0;
		for (int i = 1 ; i < inputs.length ; i++){
			if (inputs[i] > inputs[winnerIndex])
				winnerIndex = i;
		}
		weights.set(winnerIndex, 
				weights.get(winnerIndex) + alpha * (expectedOutput - output) * inputs[winnerIndex]);
		return output;
	}

	public Double process (double[] inputs){
		Double output = 0.0;
		for (int i = 0 ; i < inputs.length ; i++){
			output += weights.get(i) * inputs[i];
		}
		return output;
	}
	
	
}
