package pl.edu.agh.sn.initializestrategy;

public interface InitialValueStrategy {

	double getValue();

}
