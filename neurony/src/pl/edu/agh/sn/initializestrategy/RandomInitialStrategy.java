package pl.edu.agh.sn.initializestrategy;

import pl.edu.agh.sn.RandomValuesGenerator;

public class RandomInitialStrategy implements InitialValueStrategy {

	@Override
	public double getValue() {
		return RandomValuesGenerator.getRandomWeight();
	}

}
