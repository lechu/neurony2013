package pl.edu.agh.sn.initializestrategy;

public class ZeroInitialStrategy implements InitialValueStrategy {

	@Override
	public double getValue() {
		return 0.0;
	}

}
