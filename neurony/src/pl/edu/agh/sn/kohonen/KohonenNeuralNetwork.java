package pl.edu.agh.sn.kohonen;

import java.util.List;

import pl.edu.agh.sn.RandomValuesGenerator;

public class KohonenNeuralNetwork {

	private KohonenNeuronLayer neuronLayer;

	public KohonenNeuronLayer getOutputLayer() {
		return neuronLayer;
	}

	public KohonenNeuralNetwork(KohonenNeuronLayerBuilder builder) {
		neuronLayer = new KohonenNeuronLayer(builder);
	}

	public void train(List<LearningData> trainingData,
			TrainingConfiguration data) {

		neuronLayer.setFactorsProvider(data);

		for (int i = 0; i < data.getEpochs(); i++) {
			int randomVector = RandomValuesGenerator.nextInt(trainingData
					.size());
			double[] currentInput = trainingData.get(randomVector).getInput();

			normalizeVector(currentInput);

			int layerWinner = neuronLayer.findBestMatchingUnit(currentInput);

			neuronLayer.actualizeWeights(currentInput, layerWinner);

			data.incrementTimeStep();
		}
	}

	public double[] calculateOutput(double[] input) {
		normalizeVector(input);
		return neuronLayer.calculateOutput(input);
	}

	public void normalizeVector(double[] vector) {
		if (vector == null || vector.length == 0)
			return;

		double length = 0.0;
		for (double element : vector)
			length += element * element;

		double sqrt = Math.sqrt(length);
		double factor = sqrt == 0.0 ? 0.0 : 1 / (Math.sqrt(length));
		for (int i = 0; i < vector.length; i++)
			vector[i] *= factor;
	}

}
