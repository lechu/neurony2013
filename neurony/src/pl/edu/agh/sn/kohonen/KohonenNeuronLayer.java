package pl.edu.agh.sn.kohonen;

import java.util.Arrays;

public class KohonenNeuronLayer {

	public KohonenNeuronLayer(KohonenNeuronLayerBuilder builder) {
		this.neuronCount = builder.getNeuronCount();
		this.previousNeuronCount = builder.getPreviousNeuronCount();
		this.weights = builder.getWeights();
	}

	public double[] calculateOutput(double[] input) {

		double[] result = new double[neuronCount];
		for (int i = 0; i < result.length; i++) {
			double currentOutput = 0;
			for (int j = 0; j < input.length; j++) {
				currentOutput += input[j] * weights[i][j];
			}
			result[i] = currentOutput;
		}
		return result;
	}

	public int getNeuronCount() {
		return neuronCount;
	}

	public int getPreviousNeuronCount() {
		return previousNeuronCount;
	}

	public double[][] getWeights() {
		return copyArray(weights);
	}

	public int findBestMatchingUnit(double[] input) {
		double[] distances = calculateDistances(input);
		return min(distances);
	}

	public double[] calculateDistances(double[] input) {
		double[] result = new double[neuronCount];
		for (int i = 0; i < neuronCount; i++) {
			double[] currentWeights = getWeights()[i];
			result[i] = calculateDistance(input, currentWeights);
		}
		return result;
	}

	public double calculateDistance(double[] input, double[] weights) {
		double result = 0.0;

		for (int i = 0; i < input.length; i++) {
			double difference = (input[i] - weights[i]);
			double square = difference * difference;
			result += square;
		}
		result = Math.sqrt(result);
		return result;
	}

	public void actualizeWeights(double[] input, int winnerNeuron) {
		int radius = trainingData.getNeigbourhoodsRadius();

		for (int i = 0; i < neuronCount; i++) {
			int distance = trainingData.getNeighboorhoodStrategy().getDistance(
					winnerNeuron, i);

			if (distance <= radius) {
				double[] indexWeights = weights[i];
				for (int j = 0; j < indexWeights.length; j++) {
					double oldWeight = indexWeights[j];

					double distanceFactor = getGaussDistanceFactor(distance,
							trainingData.getNeigbourhoodsRadius());

					double learningRate = trainingData.getLearningRate();

					double difference = input[j] - indexWeights[j];

					double newWeight = oldWeight + distanceFactor
							* learningRate * difference;

					indexWeights[j] = newWeight;
				}
			}
		}
	}

	public double getGaussDistanceFactor(int distToWinner,
			int neighbourhoodRadius) {
		if (neighbourhoodRadius == 0)
			return 1;

		return Math.exp(-(distToWinner * distToWinner)
				/ (2 * neighbourhoodRadius * neighbourhoodRadius));
	}

	public TrainingConfiguration getFactorsProvider() {
		return trainingData;
	}

	public void setFactorsProvider(TrainingConfiguration configuration) {
		this.trainingData = configuration;
	}

	public double[][] copyArray(double[][] array) {
		if (array == null)
			return null;

		double[][] result = new double[array.length][];
		for (int i = 0; i < array.length; i++)
			result[i] = Arrays.copyOf(array[i], array[i].length);
		return result;
	}

	public int min(double[] array) {
		int winner = 0;
		for (int i = 0; i < array.length - 1; i++) {
			if (array[i + 1] < array[winner])
				winner = i + 1;
		}
		return winner;
	}

	private int neuronCount;
	private int previousNeuronCount;
	private double[][] weights;

	private TrainingConfiguration trainingData;

}
