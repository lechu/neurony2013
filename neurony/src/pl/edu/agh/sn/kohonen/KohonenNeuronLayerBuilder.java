package pl.edu.agh.sn.kohonen;

import java.util.Arrays;

import pl.edu.agh.sn.initializestrategy.InitialValueStrategy;

public class KohonenNeuronLayerBuilder {
	private int neuronCount;
	private int previousNeuronCount;
	protected double[][] weights;

	public int getNeuronCount() {
		return neuronCount;
	}

	public int getPreviousNeuronCount() {
		return previousNeuronCount;
	}

	public double[][] getWeights() {
		return copyArray(weights);
	}

	public double[][] copyArray(double[][] array) {
		if (array == null)
			return null;

		double[][] result = new double[array.length][];
		for (int i = 0; i < array.length; i++)
			result[i] = Arrays.copyOf(array[i], array[i].length);
		return result;
	}

	public KohonenNeuronLayerBuilder(int neuronCount, int previousNeuronCount) {
		
		this.neuronCount = neuronCount;
		this.previousNeuronCount = previousNeuronCount;
	}

	public void setWeights(double[][] weights) {
		this.weights = weights;
	}

	public void initializeWeights(InitialValueStrategy strategy) {

		weights = new double[this.neuronCount][];
		for (int i = 0; i < this.neuronCount; i++) {
			weights[i] = new double[previousNeuronCount];
		}

		for (int i = 0; i < weights.length; i++) {
			for (int j = 0; j < weights[i].length; j++) {
				weights[i][j] = strategy.getValue();
			}
		}
	}
	
}
