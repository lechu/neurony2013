package pl.edu.agh.sn.kohonen;

import java.util.Arrays;

public class LearningData {

	private double[] input;
	
	public LearningData(double[] input, double[] output) {
		this.input = input;
	}

	public double[] getInput() {
		return Arrays.copyOf(input, input.length);
	}

	public void setInput(double[] input) {
		this.input = input;
	}
}
