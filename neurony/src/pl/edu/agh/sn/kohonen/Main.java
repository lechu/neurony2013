package pl.edu.agh.sn.kohonen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.StringTokenizer;

import pl.edu.agh.sn.cp.GrossBergNeuron;
import pl.edu.agh.sn.initializestrategy.InitialValueStrategy;
import pl.edu.agh.sn.initializestrategy.RandomInitialStrategy;
import pl.edu.agh.sn.initializestrategy.ZeroInitialStrategy;
import pl.edu.agh.sn.kohonen.neighboorhood.Competition;
import pl.edu.agh.sn.kohonen.neighboorhood.Neighboorhood1D;
import pl.edu.agh.sn.kohonen.neighboorhood.Neighboorhood2D;
import pl.edu.agh.sn.kohonen.neighboorhood.NeighboorhoodStrategy;

public class Main {

	public static void main(String[] args) throws URISyntaxException,
			IOException {

		String networkType = args[0];
		String settingsFileName = args[1];
		String trainingFileName = args[2];
		String trainingConfFileName = args[3];
		String vectorFileName = args[4];

		KohonenNeuronLayerBuilder settings = parseStructureConfiguration(new File(
				settingsFileName));

		List<LearningData> data = parseTrainingData(new File(trainingFileName));

		TrainingConfiguration configuration = parseTrainingConfiguration(new File(
				trainingConfFileName));

		double[][] inputVectors = parseInputVectors(new File(vectorFileName));

		KohonenNeuralNetwork nn = getNeuralNetwork(settings);

		nn.train(data, configuration);
		
		if (networkType.equals("KOHONEN")){
			for (int i = 0; i < inputVectors.length; i++) {
				double[] currentInput = inputVectors[i].clone();
				System.out.println("Given data ("
						+ Arrays.toString(currentInput) + ")");
				double[] result = nn.calculateOutput(currentInput);
				System.out.println("Output: (" + Arrays.toString(result) + ")");
				System.out.println();
			}
		}
		else {
			String outputsFileName = args[5];
			List <Double> outputs = parseOutputsData(new File(outputsFileName));
			GrossBergNeuron grossBergNeuron= getGrossbergNeuron(settings);//one neuron only
			for (int i = 0 ; i < configuration.getEpochs() ; i++){
				for (int j = 0 ; j < data.size() ; j++)
					grossBergNeuron.teach(zeroOne(nn.calculateOutput(data.get(j).getInput())), outputs.get(j), configuration.getLearningRate());
			}
			for (int i = 0; i < inputVectors.length; i++) {
				double[] currentInput = inputVectors[i];
				System.out.println("Given data ("
						+ Arrays.toString(currentInput) + ")");
				double[] result = nn.calculateOutput(currentInput);
				System.out.println("Output: "+ grossBergNeuron.process(zeroOne(result)));
				System.out.println();
			}
			
		}
	}

	private static double[] zeroOne(double[] input) {
		int maximumIndex = 0;
		double [] output = input.clone();
		for (int i = 1 ; i < input.length ;i++){
			if (input[maximumIndex] < input [i])
				maximumIndex = i;
		}
		for (int i = 0; i < input.length ; i++){
			if (i == maximumIndex)
				output[i]=1.0;
			else
				output[i]=0.0;
		}
		return output;
	}

	private static List<Double> parseOutputsData(File file) {
		String inputString = asString(file);
		Scanner scanner = null;
		List <Double> toReturn = new ArrayList<Double>();
		try {
			scanner = new Scanner(inputString);
			while (scanner.hasNextLine()) {
				toReturn.add(scanner.nextDouble());
				scanner.nextLine();
			}
		} catch (Exception ex) {
		} finally {
			scanner.close();
		}
		return toReturn;
	}

	private static GrossBergNeuron getGrossbergNeuron(
			KohonenNeuronLayerBuilder settings) {
		return new GrossBergNeuron (settings.getNeuronCount());
	}

	public static TrainingConfiguration parseTrainingConfiguration(File file) {
		String inputString = asString(file);
		try {
			Properties props = new Properties();
			props.load(new StringReader(inputString));

			int epochs = Integer.valueOf(props.getProperty("epochs"));
			double initialLearningRate = Double.valueOf(props
					.getProperty("initialLearningRate"));
			int inputSize = Integer.parseInt(props.getProperty("inputSize"));
			int outputSize = Integer.parseInt(props.getProperty("outputSize"));

			NeighboorhoodStrategy neighbourhoodStrategy;
			String[] splitted = props.getProperty("neighbourhoodStrategy")
					.split("\\s+");
			
			String dimension = splitted[0];
			if (dimension.equals("TWO_DIMENSIONS")) {
				int a = Integer.valueOf(splitted[1]);
				int b = Integer.valueOf(splitted[2]);

				neighbourhoodStrategy = new Neighboorhood2D(a, b);
			} else if (dimension.equals("ONE_DIMENSION")) {
				neighbourhoodStrategy = new Neighboorhood1D();
			} else {
				neighbourhoodStrategy = new Competition();
			}
			TrainingConfiguration trainingConfiguration = new TrainingConfiguration(epochs,
					initialLearningRate, inputSize, outputSize, neighbourhoodStrategy);

			return trainingConfiguration;
		} catch (Exception ex) {
		}
		return null;
	}

	private static KohonenNeuronLayerBuilder parseStructureConfiguration(
			File file) {
		String inputString = asString(file);

		Scanner scanner = null;
		KohonenNeuronLayerBuilder builder = null;
		try {
			scanner = new Scanner(inputString);

			int inputVectorSize = Integer.valueOf(scanner.nextLine());

			int outputVectorSize = Integer.valueOf(scanner.nextLine());

			double[][] weights = null;
			InitialValueStrategy weightsStrategy = null;
			if (scanner.hasNext("ZERO")) {
				scanner.nextLine();
				weightsStrategy = new ZeroInitialStrategy();
			} else if (scanner.hasNext("RANDOM")) {
				scanner.next();
				scanner.nextLine();
				weightsStrategy = new RandomInitialStrategy();
			}
			if (weightsStrategy == null) {
				weights = new double[outputVectorSize][inputVectorSize];
				for (int i = 0; i < outputVectorSize; i++) {
					StringTokenizer tokenizer = new StringTokenizer(
							scanner.nextLine());

					int j = 0;
					while (tokenizer.hasMoreElements()) {
						weights[i][j++] = Double.valueOf(tokenizer.nextToken());
					}
				}
			}

			builder = new KohonenNeuronLayerBuilder(outputVectorSize,
					inputVectorSize);

			if (weightsStrategy == null) {
				builder.setWeights(weights);
			} else {
				builder.initializeWeights(weightsStrategy);
			}

		} catch (Exception ex) {
		} finally {
			scanner.close();
		}

		return builder;
	}

	private static KohonenNeuralNetwork getNeuralNetwork(
			KohonenNeuronLayerBuilder settings) {
		return new KohonenNeuralNetwork(settings);
	}

	private static List<LearningData> parseTrainingData(File file) {
		List<LearningData> output = new LinkedList<LearningData>();
		String inputString = asString(file);
		Scanner scanner = null;

		try {
			scanner = new Scanner(inputString);

			int inputVectorSize = Integer.valueOf(scanner.nextLine());

			while (scanner.hasNextLine()) {
				double[] input = new double[inputVectorSize];
				for (int j = 0; j < inputVectorSize; j++)
					input[j] = scanner.nextDouble();

				LearningData data = new LearningData(input, null);
				output.add(data);

				scanner.nextLine();
			}
		} catch (Exception ex) {
		} finally {
			scanner.close();
		}

		return output;
	}

	public static int countLines(String string) throws IOException {
		int count = 0;

		BufferedReader br = new BufferedReader(new StringReader(string));
		while (br.readLine() != null)
			count++;

		return count;
	}

	private static double[][] parseInputVectors(File file) throws IOException {
		double[][] output;

		String inputString = asString(file);

		Scanner scanner = new Scanner(inputString);
		int lines = countLines(inputString) - 1;
		int inputVectorSize = Integer.valueOf(scanner.nextLine());

		output = new double[lines][];
		for (int i = 0; i < lines; i++) {
			output[i] = new double[inputVectorSize];
			for (int j = 0; j < inputVectorSize; j++)
				output[i][j] = scanner.nextDouble();
			scanner.nextLine();
		}
		scanner.close();

		return output;
	}
	
	public static String asString(File file) {
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			StringBuilder fileContents = new StringBuilder();

			String line = "";
			while ((line = br.readLine()) != null) {
				fileContents.append(line + "\n");
			}

			return "" + fileContents;
		} catch (Exception ex) {
		} finally {
			try {
				if (br != null) {
					br.close();
				}
			} catch (Exception ex) {
			}
		}
		return null;
	}

}
