package pl.edu.agh.sn.kohonen;

import pl.edu.agh.sn.kohonen.neighboorhood.NeighboorhoodStrategy;

public class TrainingConfiguration {
	
	public TrainingConfiguration(int epochs, double initialLearningRate, 
            int inputSize, int outputSize, NeighboorhoodStrategy neighbourhoodStrategy) {
        this.timeStep = 0;
        this.epochs = epochs;
        this.initialLearningRate = initialLearningRate;
        this.inputSize = inputSize;
        this.outputSize = outputSize;
        this.neighboorhoodStrategy = neighbourhoodStrategy;
        this.deltaZero = inputSize + 1 / 2;
        this.lambda = epochs / (deltaZero == 1 ? 1 : Math.log(deltaZero));
        
    }

	public int getOutputSize() {
		return outputSize;
	}

	public void setOutputSize(int outputSize) {
		this.outputSize = outputSize;
	}

	public void incrementTimeStep() {
		this.timeStep++;
	}

	public double getLearningRate() {
		return initialLearningRate * Math.exp(-timeStep / lambda);
	}

	public int getNeigbourhoodsRadius() {
		int radius = (int) Math.round(deltaZero * Math.exp(-timeStep / lambda)) - 1;
		return radius < 0 ? 0 : radius;
	}

	public int getDeltaZero() {
		return deltaZero;
	}

	public void setDeltaZero(int deltaZero) {
		this.deltaZero = deltaZero;
	}

	public double getLambda() {
		return lambda;
	}

	public void setLambda(double lambda) {
		this.lambda = lambda;
	}

	public int getEpochs() {
		return epochs;
	}

	public void setEpochs(int epochs) {
		this.epochs = epochs;
	}

	public int getInputSize() {
		return inputSize;
	}

	public void setInputSize(int inputSize) {
		this.inputSize = inputSize;
	}

	public double getInitialLearningRate() {
		return initialLearningRate;
	}

	public void setInitialLearningRate(int initialLearningRate) {
		this.initialLearningRate = initialLearningRate;
	}

	public NeighboorhoodStrategy getNeighboorhoodStrategy() {
		return neighboorhoodStrategy;
	}

	public void setNeighboorhoodStrategy(
			NeighboorhoodStrategy neighboorhoodStrategy) {
		this.neighboorhoodStrategy = neighboorhoodStrategy;
	}

	private int deltaZero;

	private int timeStep = 0;

	private double lambda;

	private int epochs;

	private int inputSize;
	
	private int outputSize;

	private double initialLearningRate;

	private NeighboorhoodStrategy neighboorhoodStrategy;
	
}
