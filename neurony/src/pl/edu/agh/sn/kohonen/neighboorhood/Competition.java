package pl.edu.agh.sn.kohonen.neighboorhood;

public class Competition implements NeighboorhoodStrategy {

	@Override
	public int getDistance(int pointA, int pointB) {
		return pointA == pointB ? 0 : Integer.MAX_VALUE;
	}

}
