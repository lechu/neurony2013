package pl.edu.agh.sn.kohonen.neighboorhood;

public class Neighboorhood1D implements NeighboorhoodStrategy{

	@Override
	public int getDistance(int pointA, int pointB) {
		return Math.abs(pointA - pointB);
	}

}
