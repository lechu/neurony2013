package pl.edu.agh.sn.kohonen.neighboorhood;

public class Neighboorhood2D implements NeighboorhoodStrategy {

	int a;
	int b;
	
	public Neighboorhood2D(int a, int b){
		this.a = a;
		this.b = b;
	}
	
	
	@Override
	public int getDistance(int pointA, int pointB) {
        
        int[] aCoord = mapPointTo2D(pointA);
        int[] bCoord = mapPointTo2D(pointB);
        int distance = distanceBetweenTwoPoints(aCoord[0], aCoord[1], bCoord[0], bCoord[1]);
        
        return distance;
	}


	private int distanceBetweenTwoPoints(int ax, int ay, int bx, int by) {
        return (int) Math.sqrt(( bx - ax ) * ( bx - ax ) + ( by - ay )*( by - ay ));
	}


	private int[] mapPointTo2D(int point) {
		int[] result = new int[2];
        result[0] = (int) point / this.b;
        result[1] = (int) point % this.b;
        return result;
	}
	
	
}
