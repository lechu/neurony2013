package pl.edu.agh.sn.kohonen.neighboorhood;

public interface NeighboorhoodStrategy {

	int getDistance(int pointA, int pointB);
	
}
